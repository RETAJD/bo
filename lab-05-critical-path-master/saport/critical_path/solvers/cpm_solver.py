import copy

import networkx as nx
from ..model import Project
from ..project_network import ProjectState, ProjectNetwork
from typing import List, Dict
from ..solution import FullSolution


class Solver:
    '''
    A "critical path method" solver for the given project.

        Attributes:
    ----------
    project_network: ProjectNetwork
        a project network related to the given project

    Methods:
    --------
    __init__(problem: Project):
        create a solver for the given project
    solve -> FullSolution:
        solves the problem and returns the full solution
    forward_propagation() -> Dict[ProjectState,int]:
        calculates the earliest times the given events (project states) can occur
        returns a dictionary mapping network nodes to the timestamps
    backward_propagation(earliest_times: Dict[ProjectState, int]) -> Dict[ProjectState,int]:
        calculates the latest times the given events (project states) can occur
        uses earliest times to start the computation
        returns a dictionary mapping network nodes to the timestamps
    calculate_slacks(earliest_times: Dict[ProjectState, int], latest_times: Dict[ProjectState,int]) -> Dict[str, int]:
        calculates slacks for every task in the project
        uses earliest times and latest time of the events in the computations
        returns a dictionary mapping tasks names to their slacks
    create_critical_paths(slacks: Dict[str,int]) -> List[List[str]]:
        finds all the critical paths in the project based on the tasks' slacks
        returns list containing paths, every path is a list of tasks names put in the order they occur in the critical path 
    '''
    def __init__(self, problem: Project):
        self.project_network = ProjectNetwork(problem)

    def solve(self) -> FullSolution:
        earliest_times = self.forward_propagation()
        latest_times = self.backward_propagation(earliest_times)
        task_slacks = self.calculate_slacks(earliest_times, latest_times)
        critical_paths = self.create_critical_paths(task_slacks)
        # TODO:
        # set duration of the project based on the gathered data
        duration = latest_times[self.project_network.goal_node] - earliest_times[self.project_network.start_node]
        return FullSolution(duration, critical_paths, task_slacks)

    def forward_propagation(self) -> Dict[ProjectState, int]:
        # TODO:
        # earliest time of the project start node is always 0
        # every other event can occur as soon as all its predecessors plus duration of the tasks leading to the state
        #
        # earliest_times[state] = e
        earliest_times = dict()
        earliest_times[self.project_network.start_node] = 0
        for i in self.project_network.normal_nodes():
            time = 0
            for j in self.project_network.predecessors(i):
                x = earliest_times[j] + (self.project_network.arc_duration(j, i))
                if x > time:
                    time = x
            earliest_times[i] = time
        time = 0
        for j in self.project_network.predecessors(self.project_network.goal_node):
            x = earliest_times[j] + self.project_network.arc_duration(j, self.project_network.goal_node)
            if x > time:
                time = x
        earliest_times[self.project_network.goal_node] = time
        return earliest_times

    def backward_propagation(self, earliest_times: Dict[ProjectState, int]) -> Dict[ProjectState, int]:
        # TODO:
        # latest time of the project goal node always equals earliest time of the same node
        # every other event occur has to occur before its successors latest time
        latest_times = dict()
        latest_times[self.project_network.goal_node] = earliest_times[self.project_network.goal_node]
        for i in self.project_network.normal_nodes()[::-1]:
            time = float("inf")
            for j in self.project_network.successors(i):
                x = latest_times[j] - self.project_network.arc_duration(i, j)
                if x < time:
                    time = x
            latest_times[i] = time
        time = float("inf")
        for j in self.project_network.successors(self.project_network.start_node):
            x = latest_times[j] - self.project_network.arc_duration(self.project_network.start_node, j)
            if x < time:
                time = x
        latest_times[self.project_network.start_node] = time
        return latest_times

    def calculate_slacks(self, earliest_times: Dict[ProjectState, int],  latest_times: Dict[ProjectState, int]) -> Dict[str, int]:
        # TODO:
        # slack of the task equals "the latest time of its end" minus "earliest time of its start" minus its duration
        # tip: remember to ignore dummy tasks (task.is_dummy could be helpful)

        slacks = {}
        t1 = 0
        t2 = 0

        for x, y, z in self.project_network.edges():
            t1 = t2
            temp = self.project_network.arc_task(x, y)
            if temp.is_dummy:
                t1 = t1 + 1
            if t1 == t2:
                slacks[temp.name] = latest_times[y] - earliest_times[x] - temp.duration
        return slacks

    def create_critical_paths(self, slacks: Dict[str, int]) -> List[List[str]]:
        # TODO:
        # critical path start connects start node to the goal node
        # and uses only critical tasks (critical task has slack equal 0)
        # 1. create copy of the project network
        # 2. remove all the not critical tasks from the copy
        # 3. find all the paths from the start node to the goal node
        # 4. translate paths (list of nodes) to list of tasks connecting the nodes
        #
        # tip 2. use method "remove_edge(<start>, <end>" directly on the graph object 
        #        (e.g. self.project_network.network or rather its copy)
        # tip 3. nx.all_simple_paths method finds all the paths in the graph
        # tip 4. if "L" is a list "[1,2,3,4]", zip(L, L[1:]) will return [(1,2),(2,3),(3,4)]

        network_copy = copy.copy(self.project_network) # 1. create copy of the project network
        t1 = 0
        t2 = 0
        # 2. remove all the not critical tasks from the copy
        for x, y, z in network_copy.edges():
            t1 = t2
            temp = self.project_network.arc_task(x, y)
            if temp.is_dummy:
                t1 = t1 + 1
            if t1 == t2 and slacks[temp.name] != 0:
                network_copy.network.remove_edge(x, y)

        L = list(nx.all_simple_paths(network_copy.network, network_copy.start_node, network_copy.goal_node))
        critical_paths = list()

        for p in L:
            p = zip(p, p[1:])
            tasks = [network_copy.arc_task(x, y) for x, y in p]
            tasks = [temp.name for temp in tasks if not temp.is_dummy ]
            critical_paths.append(tasks)

        return critical_paths
