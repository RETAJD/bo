from ..model import Project
from ..project_network import ProjectNetwork
from ...simplex.model import Model
from ...simplex.expressions.expression import Expression
from ..solution import BasicSolution


class Solver:
    '''
    Simplex based solver looking for the critical path in the project.
    Uses linear model maximizing length of the path in the project network. 

    Attributes:
    ----------
    project_network: ProjectNetwork
        a project network related to the given project
    model: simplex.model.Model
        a linear model looking for the maximal path in the project network
    Methods:
    --------
    __init__(problem: Project)
        create a solver for the given project
    create_simplex_model() -> simplex.model.Model
        builds a linear model of the problem
    solve() -> BasicSolution
        finds the duration of the critical (longest) path in the project network
    '''
    def __init__(self, problem: Project):
        self.project_network = ProjectNetwork(problem)
        self.model = self.create_simplex_model()

    def create_simplex_model(self) -> Model:
        # TODO:
        # 0) we need as many variables as there is edges in the project network
        # 1) every variable has to be <= 1
        # 2) sum of the variables starting at the initial state has to be equal 1
        # 3) sum of the variables ending at the goal state has to be equal 1
        # 4) for every other node, total flow going trough it has to be equal 0
        #    i.e. sum of incoming arcs minus sum of the outgoing arcs = 0
        # 5) we have to maximize length of the path
        #    (sum of variables weighted by the durations of the corresponding tasks)
        model = Model("critical path (max)")
        starting = Expression()
        ending = Expression()
        objective = Expression()
        for i in self.project_network.edges():
            x = model.create_variable(f"x{i[0].index}{i[1].index}")
            model.add_constraint(x <= 1)
            objective += self.project_network.arc_duration(i[0], i[1]) * x
            if i[0].index == 1:
                starting = starting + x
            elif i[1].index == self.project_network.goal_node.index:
                ending += x
        model.add_constraint(starting == 1)
        model.add_constraint(ending == 1)
        model.maximize(objective)
        for i in self.project_network.normal_nodes():
            expr = Expression()
            for j in self.project_network.predecessors(i):
                for x in model.variables:
                    if x.name[1] == str(j.index) and x.name[2] == str(i.index):
                        expr += x
            for k in self.project_network.successors(i):
                for x in model.variables:
                    pass
                    if x.name[1] == str(i.index) and x.name[2] == str(k.index):
                        expr -= x
            model.add_constraint(expr == 0)
        return model

    def solve(self) -> BasicSolution:
        solution = self.model.solve()
        return BasicSolution(int(solution.objective_value()))
