import logging
from saport.simplex.model import Model 

from saport.simplex.model import Model

def create_model() -> Model:
    model = Model("example_05_infeasible")
    #TODO:
    # fill missing test based on the example_03_unbounded.py
    # to make the test a bit more interesting:
    # * make sure model is infeasible

    x1 = model.create_variable("x1")
    x2 = model.create_variable("x2")

    model.add_constraint(45*x1 - 33*x2 <= -35)
    model.add_constraint(2*x1 + 35*x2 == 122)
    model.add_constraint(325*x1 + 35*x2 >= 11110)

    model.maximize(31* x1 + 280 * x2)

    return model

def run():
    #TODO:
    # add a test "assert something" based on the example_01_solvable.py
    # TIP: you may use other solvers (e.g. https://online-optimizer.appspot.com)
    #      to find the correct solution
    model = create_model()

    solution = model.solve()
    if solution.is_feasible:
        raise AssertionError("Your algorithm found a solution to an unbounded problem. This shouldn't happen...")
    else:
        logging.info("Congratulations! This problem is infeasible and your algorithm has found that :)")

if __name__ == "__main__":
    logging.basicConfig(level=logging.INFO, format='%(message)s')
    run()
