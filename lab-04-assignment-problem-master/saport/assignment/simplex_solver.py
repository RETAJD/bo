import numpy as np
from .model import AssignmentProblem, Assignment, NormalizedAssignmentProblem
from ..simplex.model import Model
from ..simplex.expressions.expression import Expression
from dataclasses import dataclass
from typing import List 



class Solver:
    '''
    A simplex solver for the assignment problem.

    Methods:
    --------
    __init__(problem: AssignmentProblem):
        creates a solver instance for a specific problem
    solve() -> Assignment:
        solves the given assignment problem
    '''
    def __init__(self, problem: AssignmentProblem):
        self.problem = NormalizedAssignmentProblem.from_problem(problem)

    def solve(self) -> Assignment:
        model = Model("assignment")
        # TODO:
        # 1) creates variables, one for each cost in the cost matrix
        # 2) add constraint, that sum of every row has to be equal 1
        # 3) add constraint, that sum of every col has to be equal 1
        # 4) add constraint, that every variable has to be <= 1
        # 5) create an objective expression, involving all variables weighted by their cost
        # 6) add the objective to model (minimize it!)

        objective = Expression()
        rzedy, kolum = self.problem.costs.shape
        for x in range(rzedy):
            for y in range(kolum):
                wart = model.create_variable(f"x{x}{y}")
                model.add_constraint(wart <= 1)
                t = self.problem.costs[x, y] * wart
                objective = objective + t
        model.objective = objective
        model.minimize(model.objective)

        for x in range(rzedy):
            exp = Expression()
            for i in range(x*kolum, (x+1)*kolum):
                exp += model.variables[i]
            model.add_constraint(exp == 1)
        for y in range(kolum):
            exp1 = Expression()
            for i in range(y, rzedy * kolum, kolum):
                exp1 += model.variables[i]
            model.add_constraint(exp1 == 1)
        solution = model.solve()
        # TODO:
        # 1) extract assignment for the original problem from the solution object
        # tips:
        # - remember that in the original problem n_workers() not alwyas equals n_tasks()

        assigned_tasks = [-1 for j in range(rzedy)]
        for i in range(rzedy):
            for x in range(i * kolum, (i+1)*kolum):
                if solution.assignment()[x] == 1 and i <= (self.problem.original_problem.costs.shape[0] - 1) and (x - i * kolum) <= (self.problem.original_problem.costs.shape[1] - 1):
                    assigned_tasks[i] = x - i * kolum
        org_objective = 0
        for i in range(len(assigned_tasks)):
            if i <= (self.problem.original_problem.costs.shape[0] - 1) and (assigned_tasks[i]) <= (self.problem.original_problem.costs.shape[1] - 1)\
                    and assigned_tasks[i] >= 0:
                org_objective += self.problem.original_problem.costs[i][assigned_tasks[i]]

        return Assignment(assigned_tasks, org_objective)



