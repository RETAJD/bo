import numpy as np
from .model import Assignment, AssignmentProblem, NormalizedAssignmentProblem
from typing import List, Dict, Tuple, Set
from numpy.typing import ArrayLike

class Solver:
    '''
    A hungarian solver for the assignment problem.

    Methods:
    --------
    __init__(problem: AssignmentProblem):
        creates a solver instance for a specific problem
    solve() -> Assignment:
        solves the given assignment problem
    extract_mins(costs: ArrayLike):
        substracts from columns and rows in the matrix to create 0s in the matrix
    find_max_assignment(costs: ArrayLike) -> Dict[int,int]:
        finds the biggest possible assinments given 0s in the cost matrix
        result is a dictionary, where index is a worker index, value is the task index
    add_zero_by_crossing_out(costs: ArrayLike, partial_assignment: Dict[int,int])
        creates another zero(s) in the cost matrix by crossing out lines (rows/cols) with zeros in the cost matrix,
        then substracting/adding the smallest not crossed out value
    create_assignment(raw_assignment: Dict[int, int]) -> Assignment:
        creates an assignment instance based on the given dictionary assignment
    '''
    def __init__(self, problem: AssignmentProblem):
        self.problem = NormalizedAssignmentProblem.from_problem(problem)

    def solve(self) -> Assignment:
        costs = np.array(self.problem.costs)

        while True:
            self.extracts_mins(costs)
            max_assignment = self.find_max_assignment(costs)
            if len(max_assignment) == self.problem.size():
                return self.create_assignment(max_assignment)
            self.add_zero_by_crossing_out(costs, max_assignment)

    def extracts_mins(self, costs: ArrayLike):
        #TODO: substract minimal values from each row and column
        row, col = np.shape(costs)
        for i in range(row):
            minimum = costs[i].min()
            costs[i] -= minimum
        for j in range(col):
            minimum = costs[:, j].min()
            costs[:, j] -= minimum

    def add_zero_by_crossing_out(self, costs: ArrayLike, partial_assignment: Dict[int, int]):
        #TODO:
        # 1) "mark" columns and rows according to the instructions given by teacher
        # 2) cross out marked columns and not marked rows
        # 3) find minimal uncrossed value and subtract it from the cost matrix
        # 4) add the same value to all crossed out columns and rows

        # print('add zero')
        # print(costs)
        # costs = np.array([[1, 0, 0, 0], [0, 1, 1, 0],[3, 0, 0, 2], [4, 5, 1, 0]])
        # partial_assignment = {0: 2, 1: 3, 2: 1}
        row, col = np.shape(costs)
        marked_rows = {x for x in range(row) if x not in partial_assignment.keys()}
        marked_columns = set()
        while True:
            extra_columns = set()
            extra_columns = {x for x in range(col) if x not in marked_columns and len([costs[y, x] for y in marked_rows if costs[y, x] == 0]) > 0}
            if extra_columns == set():
                break
            marked_columns = marked_columns.union(extra_columns)
            extra_rows = set()
            extra_rows = {y for y in range(row) if y not in marked_rows and len([x for x in marked_columns if partial_assignment[y] == x])}
            if extra_rows == set():
                break
            marked_rows = marked_rows.union(extra_rows)
        minimall = min([costs[x, y] for x, y in np.ndindex(costs.shape) if x in marked_rows and y not in marked_columns])
        costs -= minimall
        for x in range(row):
            if x not in marked_rows:
                costs[x, :] += minimall
        for y in marked_columns:
            costs[:, y] += minimall

    def find_max_assignment(self, costs) -> Dict[int,int]:
        partial_assignment = dict()
         #TODO: find the biggest assignment in the cost matrix
        # 1) always try first the row with the least amount of 0s
        # 2) then use column with the least amount of 0s
        # TIP: remember, rows and cols can't repeat in the assignment
        #      partial_assignment[1] = 2 means that the worker with index 1
        #                                has been assigned to task with index 2
        # costs = np.array([[1, 8, 1, 0], [0, 5, 0, 1], [6, 2, 4, 0], [0, 0, 4, 2]])
        array = np.array(costs)
        rows, columns = np.shape(array)
        row = np.zeros((rows,), dtype=int)
        col = np.zeros((rows,), dtype=int)
        for i in range(rows):
            for j in range(columns):
                if array[i][j] == 0:
                    row[i] += 1
                    col[j] += 1
        for x in range(rows):
            z = np.where(row == row.min())
            if row[z[0][0]] > rows:
                break
            worker = z[0][0]
            job = -1
            mini = columns + 1
            for i in range(columns):
                if array[worker][i] == 0 and col[i] < mini:
                    job = i
                    mini = col[i]
            if job == -1:
                break
            for j in range(rows):
                if array[j][job] == 0:
                    array[j][job] = 10
                    row[j] -= 1
                    col[job] -= 1
                    if col[job] == 0:
                        col[job] = columns
                    if row[j] == 0:
                        row[j] = rows + 2
            for i in range(columns):
                if array[worker][i] == 0:
                    array[worker][i] = 10
                    col[i] -= 1
                    if col[i] == 0:
                        col[i] = columns
            partial_assignment[worker] = job
            row[worker] = rows + 2
        # if self.problem.original_problem.name == 'square_max_04_03_24':
        #     print(self.problem.original_problem.name)
        #     print(costs)
        #     print(self.problem.original_problem.costs)
        #     print(partial_assignment)
        return partial_assignment

    def create_assignment(self, raw_assignment: Dict[int,int]) -> Assignment:
        #TODO: create an assignment instance based on the dictionary
        # tips:
        # 1) use self.problem.original_problem.costs to calculate the cost
        # 2) in case the original cost matrix (self.problem.original_problem.costs wasn't square)
        #    and there is more workers than task, you should assign -1 to workers with no task

        costs = self.problem.original_problem.costs
        assignment = [-1 for i in range(costs.shape[0])]
        total_cost = 0
        for i in raw_assignment:
            if i <= (self.problem.original_problem.costs.shape[0] - 1) and raw_assignment[i] <= (self.problem.original_problem.costs.shape[1] - 1):
                assignment[i] = raw_assignment[i]
        for i in range(len(assignment)):
            if assignment[i] >= 0:
                total_cost += costs[i][assignment[i]]
        return Assignment(assignment, total_cost)
