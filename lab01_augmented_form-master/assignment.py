from saport.simplex.model import Model

def assignment_1():
    model = Model("Assignment 1")

    #TODO:
    # Add:
    # - variables
    # - constraints
    # - objective

    x1 = model.create_variable("x1")
    x2 = model.create_variable("x2")
    x3 = model.create_variable("x3")

    model.add_constraint(x1 + x2 + x3 <= 30)
    model.add_constraint(x1 + 2 * x2 + x3 >= 10)
    model.add_constraint(2 * x2 + x3 <= 20)

    model.maximize(2 * x1 + x2 + 3 * x3)

    return model

def assignment_2():
    model = Model("Assignment 2")

    #TODO:
    # Add:
    # - variables
    # - constraints
    # - objective

    x1 = model.create_variable("x1")
    x2 = model.create_variable("x2")
    x3 = model.create_variable("x3")
    x4 = model.create_variable("x4")

    model.add_constraint(0.8 * x1 + 2.4 * x2 + 0.9 * x3 + 0.4 * x4 >= 1200)
    model.add_constraint(0.6 * x1 + 0.6 * x2 + 0.3 * x3 + 0.3 * x4 >= 600)

    model.minimize(9.6 * x1 + 14.4 * x2 + 10.8 * x3 + 7.2 * x4)

    return model

def assignment_3():
    model = Model("Assignment 3")

    x1= model.create_variable("x1")
    x2= model.create_variable("x2")

    model.add_constraint(5 * x1 + 15 * x2 >= 50)
    model.add_constraint(20 * x1 + 5 * x2 >= 40)
    model.add_constraint(15 * x1 + 2 * x2 <= 60)

    model.minimize(8 * x1 + 4 * x2)

    return model

def assignment_4():
    model = Model("Assignment 4")

    x1 = model.create_variable("x1")
    x2 = model.create_variable("x2")
    x3 = model.create_variable("x3")
    x4 = model.create_variable("x4")
    x5 = model.create_variable("x5")
    x6 = model.create_variable("x6")
    x7 = model.create_variable("x7")
    x8 = model.create_variable("x8")
    x9 = model.create_variable("x9")
    x10 = model.create_variable("x10")
    x11 = model.create_variable("x11")
    x12 = model.create_variable("x12")
    x13 = model.create_variable("x13")
    x14 = model.create_variable("x14")
    x15 = model.create_variable("x15")

    model.add_constraint(x1 + x2 + x3 + x4 >= 150)
    model.add_constraint(x2 + x5 + x6 + x7 + x8 + 2 * x9 + 2 * x10 >= 200)
    model.add_constraint(x3 + 2 * x4 + x6 + 2 * x7 + 3 * x8 + 1 * x10 + 2 * x11 + 3 * x12 + 4 * x13 + 5 * x14 >= 150)

    model.minimize(95 * x1 + 20 * x2 + 60 * x3 + 25 * x4 + 125 * x5 + 90 * x6 + 55 * x7 + 20 * x8+ 50 * x9 + 15 * x10 + 130 * x11 + 95 * x12 + 60 * x13 + 25 * x14)

    return model