# Lab 07 - Integer Programming

The goal of this is lab is to implement to general Integer Programming algorithms and apply them to the knapsack problem:

* fill missing code in: 
  * `saport.knapsack.solvers.integer_implicit_enumeration`
  * `saport.knapsack.solvers.integer_linear_relaxation`
  * `saport.integer.solvers.implicit_relaxation`
  * `saport.integer.solvers.linear_relaxation`

* you may use the `benchmark.py` to compare your generic implementations with the knapsack specific ones

## SAPORT

SAPORT = Student's Attempt to Produce an Operation Research Toolkit

Includes:

* two-step simplex
* knapsack solver
* integer programming: linear relaxation and implicit enumeration