Dear Student,

I'm happy to announce that you've managed to get **6** out of 10 points for this assignment.\
There still exist some issues, I'm obliged to point out:

<details><summary>objective has to be defined before simplifying the model</summary></details>
<details><summary>Branch and bound in the implicit enumeration algorithm fails for the following test cases:</summary>ks_lecture_dp_1<br>ks_lecture_dp_2<br>ks_4_0<br>ks_7_test<br>ks_8_test</details>
<details><summary>ImplicitEnumerationSolver sometimes fails to properly assess if partial assignment is satisfiable:</summary>Test case 'ks_lecture_dp_1' succeeds in 0%.<br>Test case 'ks_lecture_dp_2' succeeds in 0%.<br>Test case 'ks_4_0' succeeds in 0%.<br>Test case 'ks_7_test' succeeds in 0%.<br>Test case 'ks_8_test' succeeds in 0%.</details>
<details><summary>Solver should find variable with least infeasibility >> min() arg is an empty sequence</summary></details>

-----------
I remain your faithful servant\
_Bobot_