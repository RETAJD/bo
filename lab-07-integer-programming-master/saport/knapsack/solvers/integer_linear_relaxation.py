from saport.knapsack.abstractsolver import AbstractSolver
from saport.knapsack.model import Problem, Solution, Item
from typing import List
from saport.integer.model import Model
from saport.integer.solvers.linear_relaxation import LinearRelaxationSolver
from saport.simplex.expressions.expression import Expression


class IntegerLinearRelaxationSolver(AbstractSolver):
    """
    An Integer Programming solver for the knapsack problems

    Methods:
    --------
    create_model() -> Model:
        creates and returns an integer programming model based on the self.problem
    """
    def create_model(self) -> Model:
        m = Model('knapsack')
        # TODO:
        # - variables: whether the item gets taken
        # - constraints: weights
        # - objective: values
        constraint = Expression()
        objective = Expression()
        for item in self.problem.items:
            x = m.create_variable(f'x{item.index}')
            m.add_constraint(x <= 1)
            m.add_constraint(x >= 0)
            constraint += item.weight * x
            objective += item.value * x
        m.add_constraint(constraint <= self.problem.capacity)
        m.objective = objective
        m.maximize(m.objective)
        return m

    def _solving_routine(self) -> Solution:
        m = self.create_model()
        solver = LinearRelaxationSolver()
        integer_solution = solver.solve(m, self.timelimit)
        items = [item for (i, item) in enumerate(self.problem.items) if integer_solution.value(m.variables[i]) > 0]
        solution = Solution.from_items(items, not solver.interrupted)
        return solution