from copy import deepcopy
from saport.integer.abstract_solver import AbstractIntegerSolver
from saport.simplex import solver as lpsolver
from saport.integer.model import Model
from saport.integer.solution import Solution
from saport.simplex.expressions import constraint as ssecon
from saport.simplex.expressions import expression as sseexp
from saport.simplex import solution as lpsolution
import math

class LinearRelaxationSolver(AbstractIntegerSolver):
    """
        Naive branch and bound solver for general integer programming problems
        using a linear relaxation approach. 
    """  

    def _solving_routine(self):
        self._branch_and_bound(self.model)
        self.best_solution = Solution.with_linear_solution(self.model, self.best_solution, not self.interrupted)
           
    def _branch_and_bound(self, model: Model):
        #TODO: implement a branch and bound procedure analogically to the one in the knaspack solver
        #      but use the simplex to relax the problem
        #
        # tip 1: use self.find_float_assignment to select the branching variable
        # tip 2: use self._model_with_new_constraint to extend model with a new constraint
        # tip 3: remember to check for the timeout and set the self.interrupted to True when it happens
        # tip 4: remember to handle infeasible and unbounded models, is_unbounded / is_feasible / has_assignemnt methods may be handy

        if self.timeout():
            self.interrupted = True
            return
        solution = lpsolver.ssmod.Model.solve(model)
        if not solution.has_assignment():
            return
        variable = self._find_float_assignment(solution)
        if variable is None:
            if self.best_solution is None or solution.objective_value() > self.best_solution.objective_value():
                self.best_solution = solution
            return
        number = int(solution.assignment(model)[variable.index])
        self._branch_and_bound(self._model_with_new_constraint(model, variable <= number))
        number += 1
        self._branch_and_bound(self._model_with_new_constraint(model, variable >= number))
        
    def _find_float_assignment(self, solution: lpsolution.Solution) -> sseexp.Variable:
        #TODO: find an variable that has non-integer value in the solution
        # tip: due to numeric errors some variables can have "almost integer" value, like 0.9999999 or 1.0000001
        #      make sure they are still counted as integers
        for variable in reversed(self.model.variables):
            if solution.value(variable) != float(int(solution.value(variable))):
                return variable
        return None

    def _model_with_new_constraint(self, model: Model, constraint: ssecon.Constraint) -> Model:
        new_model = deepcopy(model)
        new_model.add_constraint(constraint)
        return new_model
