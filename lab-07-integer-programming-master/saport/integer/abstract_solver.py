from copy import deepcopy
from saport.simplex import solver as lpsolver
from saport.integer.model import Model
from saport.integer.solution import Solution
from saport.simplex.expressions import constraint as ssecon
from saport.simplex.expressions import expression as sseexp
from saport.simplex import solution as lpsolution
import math
import time 

class AbstractIntegerSolver:
    """
        Abstract Integer Programming Solver

        Attributes
        ----------
        model : Model
            integer programming model to be solved
        timelimit: int
            what is the maximal solving time (in seconds)
        total_time: float
            how long it took to solve the problem
        start_time: float
            when the solving started
        interrupted: bool
            whether solving has been interrupted (by timeout)

        Methods
        -------
        start_timer():
            remember the starting time for the solver
        stop_timer():
            stores the total solving time
        wall_time() -> float:
            returns how long solver has been working
        timeout() -> bool:
            whether solver should stop working due to the timeout

        solve(model: Model, timelimit: int) -> Solution:
            solves the given model within a specified timelimit
        _solving
    """  

    def solve(self, model: Model, timelimit: int) -> Solution:
        self.timelimit = timelimit
        self.total_time = None
        self.start_time = None
        self.interrupted = False

        self.model = model
        self.best_solution = None

        self.start_timer()
        self._solving_routine()
        self.stop_timer()

        return self.best_solution
           
    def _lower_bound(self):
        return self.best_solution.objective_value() if self.best_solution != None and self.best_solution.has_assignment() else float('-inf') 

    def _solving_routine(self):
        raise Exception("abstract solver shouldn't be called!")

    def start_timer(self):
        self.start_time = time.time()

    def stop_timer(self):
        self.total_time = self.wall_time()

    def wall_time(self) -> float:
        return time.time() - self.start_time

    def timeout(self) -> bool:
        return self.wall_time() > self.timelimit
